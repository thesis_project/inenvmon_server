import os
import sys
import threading
import subprocess
import signal

class A_process(object):
    def __init__(self, name, cmd):
        self.name = name
        self.cmd = cmd

    def run(self):
        output = ""
        self.popen = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        while self.popen.poll() is None:
            line = self.popen.stdout.readline().rstrip()
            if line:
                output += "{}\n".format(line)
                print(line)
                sys.stdout.flush()
        self.popen.wait()

    def signal_handler(self, signum, frame):
        print("INFO: {}: Received signal {}".format(self.name, signum))
        print("Terminating {} {}".format(self.name, self.cmd))
        self.popen.terminate()
        sys.exit(0)

def main():
    print("INFO: Starting Mosquitto MQTT broker")
    mosquitto_run = threading.Thread(target=A_process("Mosquitto", ["/usr/sbin/mosquitto", "-c", "/etc/mosquitto/mosquitto.conf"]).run,
                                    name="Mosquitto MQTT broker",
                                    args=(), daemon=True)
    mosquitto_run.start()

    print("INFO: Starting MQTT data collector")
    collector_run = threading.Thread(target=A_process("Collector", ["python3", "/inenvmon/inenvmon_collector.py"]).run,
                                    name="MQTT listener and data collector",
                                    args=(), daemon=True)
    collector_run.start()

    print("INFO: Starting inenvmon web app and API")
    inenvmon_proc = A_process("Inenvmon_web", ["python3", "/inenvmon/web/inenvmon_web.py"])
    signal.signal(signal.SIGINT, inenvmon_proc.signal_handler)
    signal.signal(signal.SIGTERM, inenvmon_proc.signal_handler)
    inenvmon_proc.run()

if __name__ == "__main__":
    main()
