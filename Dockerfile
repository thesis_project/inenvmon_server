FROM debian:stable

RUN mkdir -p /inenvmon/data && apt-get update && apt-get install python3 python3-pip mosquitto -y && python3 -m pip install poetry --quiet && python3 -m poetry config virtualenvs.create false
COPY mosquitto_users /etc/mosquitto/passwd
COPY mosquitto.conf /etc/mosquitto/conf.d/default.conf
COPY inenvmon/pyproject.toml /inenvmon/pyproject.toml
RUN mkdir /var/run/mosquitto && chown mosquitto: /var/run/mosquitto && mosquitto_passwd -U /etc/mosquitto/passwd && cd /inenvmon && python3 -m poetry install

EXPOSE 8080
EXPOSE 9888
WORKDIR /inenvmon
CMD python3 /inenvmon/start.py

