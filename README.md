# inenvmon_server

Serverside stuff for thesis project.
Some components are under a different license.

The provided Dockerfile builds a monolith container providing an MQTT broker, data collector, data access API and a webapp.

Passwords should be set up before deploying the container. The MQTT password should substitute the string `mqttsecret` in `inenvmon/inenvmon_collector.py` and `mosquitto_users` and the API secret should substitute the string `itsasecret` in `inenvmon/inenvmon_collector.py` and `inenvmon/web/inenvmon_web.py`

The MQTT broker is running on port 9888 and the API/webapp on port 8080. Mapping to host ports can be adjusted in docker-compose.yml

The legacy version can be found in the `legacy` branch of this repository.
